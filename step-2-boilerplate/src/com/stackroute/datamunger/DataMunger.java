package com.stackroute.datamunger;

import java.util.List;
import java.util.Scanner;

import com.stackroute.datamunger.query.parser.AggregateFunction;
import com.stackroute.datamunger.query.parser.QueryParameter;
import com.stackroute.datamunger.query.parser.QueryParser;
import com.stackroute.datamunger.query.parser.Restriction;

public class DataMunger {

	public static void main(String[] args) {

		
		//read the query from the user
		System.out.println("Inout the query: ");
		Scanner scanner=new Scanner(System.in);
		String queryString=scanner.nextLine();
		//create an object of QueryParser class
		QueryParser queryParser=new QueryParser();
		//call parseQuery() method of queryParser
		DataMunger dataMunger=new DataMunger();
		dataMunger.display(queryString, queryParser.parseQuery(queryString));
		
	}
	
	private void display(String queryString, QueryParameter queryParameter) {
		System.out.println("\nQuery : " + queryString);
		System.out.println("--------------------------------------------------");
		System.out.println("Base Query:" + queryParameter.getBaseQuery());
		System.out.println("File:" + queryParameter.getFile());
		System.out.println("Query Type:" + queryParameter.getQUERY_TYPE());
		List<String> fields = queryParameter.getFields();
		System.out.println("Selected field(s):");
		if (fields == null || fields.isEmpty()) {
			System.out.println("*");
		} else {
			for (String field : fields) {
				System.out.println("\t" + field);
			}
		}
		
		List<Restriction> restrictions = queryParameter.getRestrictions();
		
		if(restrictions!=null && !restrictions.isEmpty())
		{
			System.out.println("Where Conditions : ");
			int conditionCount=1;
			for(Restriction restriction :restrictions )
			{
				System.out.println("\tCondition : " + conditionCount++);
				System.out.println("\t\tName : "+restriction.getPropertyName());
				System.out.println("\t\tCondition : "+restriction.getCondition());
				System.out.println("\t\tValue : "+restriction.getPropertyValue());
			}
		}
		List<AggregateFunction>  aggregateFunctions = queryParameter.getAggregateFunctions();
		if(aggregateFunctions!=null && !aggregateFunctions.isEmpty()){
			
			System.out.println("Aggregate Functions : ");
			int funtionCount=1;
			for(AggregateFunction aggregateFunction :aggregateFunctions )
			{
				System.out.println("\t Aggregate Function : " + funtionCount++);
				System.out.println("\t\t function : "+aggregateFunction.getFunction());
				System.out.println("\t\t  field : "+aggregateFunction.getField());
			}
			
		}
		
		List<String>  orderByFields = queryParameter.getOrderByFields();
		if(orderByFields!=null && !orderByFields.isEmpty()){
			
			System.out.println(" Order by fields : ");
			for(String orderByField :orderByFields )
			{
				System.out.println("\t "+orderByField);
				
			}
			
		}
		
		List<String>  groupByFields = queryParameter.getGroupByFields();
		if(groupByFields!=null && !groupByFields.isEmpty()){
			
			System.out.println(" Group by fields : ");
			for(String groupByField :groupByFields )
			{
				System.out.println("\t "+groupByField);
				
			}
			
		}
	}
	


}
